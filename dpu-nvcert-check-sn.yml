###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


###############################################################################
# This playbook
# - Pulls the SN from the DPU BMC and checks if it matches any of the devices
#   hosted by the server
# - Pulls the SN from the DPU Arm and checks if it matches the SN from the DPU
#   BMC
###############################################################################
- hosts: dpu_bmcs
  become: yes
  gather_facts: no
  tasks:
    - name:
      include_role:
        name: nc-get-dpu-sn-from-bmc

    - name: Print the DPU SN from the BMC inventory
      debug:
        msg: "{{ dpu_sn_from_bmc }}"

    - name:
      include_role:
        name: nc-match-dpu-bmc-to-host

    - name: Print the DPU SN from the host
      debug:
        msg: "{{ dpu_sn_from_host }}"

    - name: Check if the DPU BMC is hosted by this server
      debug:
        msg: "Abort play; DPU BMC {{ 'dpu_sn_from_bmc' }} is not hosted by this server"
      when: dpu_sn_from_host == 'unknown'
      failed_when: dpu_sn_from_host == 'unknown'

    - name:
      include_role:
        name: nc-get-dpu-sn

    - name: Print the DPU SN from the DPU Arm
      debug:
        msg: "{{ dpu_sn_from_dpu }}"

    - name: Check if the DPU BMC matches the DPU
      debug:
        msg: "Abort play; SN mismatch DPU BMC {{ dpu_sn_from_bmc }} and DPU {{ dpu_sn_from_dpu }}"
      when: dpu_sn_from_dpu != dpu_sn_from_bmc
      failed_when: dpu_sn_from_dpu != dpu_sn_from_bmc

    - name: Serial numbers from the Host, DPU-BMC, DPU-Arms
      debug:
        msg: "x86: {{ dpu_sn_from_host }}, dpu-bmc: {{ dpu_sn_from_bmc }}, dpu-arm: {{ dpu_sn_from_dpu }}"
