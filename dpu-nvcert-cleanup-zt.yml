###############################################################################
#
# Copyright 2023 NVIDIA Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
###############################################################################


# This playbook restores the DPU to a factory default state

# 1. Put the DPU back in the default embedded mode where the host is trusted
# 2. Reset the DPU BMC password to the factory default
- hosts: dpus
  become: yes
  tasks:
    # Get the mst device id
    - name: Role - Get mst name
      import_role: 
        name: nc-get-dpu-mst-dev-id

    - name: Set the DPU in ECPF mode
      shell: 'mlxprivhost -d {{ mst_dev_id }} p'# noqa 305

    - name: Reset the NIC config to default
      import_role:
        name: nc-reset-dpu-default-config

    - name: Factory reset the DPU BMC configuration
      shell: 'ipmitool raw 0x32 0x66' # noqa 305

    - name: Cold reset the DPU BMC
      shell: 'ipmitool mc reset cold' # noqa 305

    - name: Wait 5 minutes for DPU BMC reset
      pause:
        minutes: 5


# Powercycle the host after restoring the DPU settings
- hosts: x86_hosts
  become: yes
  pre_tasks:
    - name: Enable x86 reboot
      set_fact:
        x86_reboot: True
  roles:
    - role: nc-reboot-host
